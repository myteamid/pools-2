// sliders.js
jQuery(function ($) {
// arrow icons
  var arrowLeft = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
  var arrowRight = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
  // preset options
  var heroSlider = $('.js-shares-slider');
  var heroOption = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true,
    nextArrow: '<button type="button" class="slider_btn--next slider_btn"> ' + arrowRight + ' </button>',
    prevArrow: '<button type="button" class="slider_btn--prev slider_btn">' + arrowLeft + '</button>',
    dots: false,
    autoplay: true,
    centerMode: true,
    autoplaySpeed: 5000,
    speed: 500,
    infinite: true,
    fade: false,
    cssEase: 'linear',
    accessibility: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };

  if ($('.shares-slider_slide').length > 3) {
    // init slider
    heroSlider.slick(heroOption);
  }

}); // ready
// sliders.js end