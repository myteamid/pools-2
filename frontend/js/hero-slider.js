// sliders.js
jQuery(function ($) {
// arrow icons
  var arrowLeft = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
  var arrowRight = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
  // preset options
  var heroSlider = $('.js-hero-slider');
  var heroOption = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '<button type="button" class="slider_btn--next slider_btn"> ' + arrowRight + ' </button>',
    prevArrow: '<button type="button" class="slider_btn--prev slider_btn">' + arrowLeft + '</button>',
    dots: false,
    // autoplay: true,
    // autoplaySpeed: 3000,
    speed: 500,
    infinite: true,
    fade: false,
    cssEase: 'linear',
    accessibility: false // prevent page scroll up on autoplay
  };

  // init slider
  heroSlider.slick(heroOption);

}); // ready
// sliders.js end